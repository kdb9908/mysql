#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

#update system
sudo yum -y update

#install mysql
sudo yum -y install mysql-server
sudo echo "[mysql]
user=root
password=
[mysqldump]
user=root
password=" > ~/.my.cnf
sudo service mysqld start
sudo mysql < /tmp/superveda-db-init.sql
sudo mysql < /tmp/acme-db-init.sql
sudo service mysqld restart


