CREATE DATABASE acmedb;
CREATE USER acme;
GRANT ALL ON acmedb.* TO 'acme'@'%' IDENTIFIED BY 'password';

USE acmedb;
